import asyncio
import os

from fastapi import FastAPI
from fastapi.responses import Response

from cam_control import Cameras

app = FastAPI()

cameras = Cameras()


@app.get("/take/{cam_id}/{exposure}")
async def main(cam_id: int, exposure: int):
    data = cameras.take_image(cam_id, "jpeg", exposure)
    try:
         return Response(data.read(), media_type="image/jpeg")
    except:
         return 0

@app.get("/raw/{cam_id}/{exposure}")
async def main(cam_id: int, exposure: int):
    data = cameras.take_image(cam_id, "rgb", exposure)
    return Response(data.read(), media_type="image/rgb")
