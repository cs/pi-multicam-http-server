"""
### BEGIN NODE INFO
[info]
name = PiCam1
version = 1.2
description =

[startup]
cmdline = %PYTHON% %FILE%
timeout = 30

[shutdown]
# message = 987654321
# setting = kill
timeout = 30
### END NODE INFO
"""

from labrad import units
from labrad.server import LabradServer, setting, Signal

class PiCameraServer(LabradServer):
    name = "Pi Camera Server 1"
    
    @inlineCallbacks
    def initServer(self):
        self.camera = None
        
    @setting(1, 'config_and_connect', exposure_time='v')
    def config_and_connect(self, exposure_time=1000):
        if self.camera is None:
             self.camera = PiCamera()
        
        self.camera.shutter_speed = exposure_time
        self.camera.exposure_mode = 'off'
        g = self.camera.awb_gains
        self.camera.awb_mode = 'off'
        self.camera.awb_gains = g
             
    @setting(2, 'capture', returns=['*3w', '(s(www)y)'])
    def capture(self):
        res = self.camera.resolution
        output = np.empty((res[0], res[1], 3), dtype=np.uint8)
        self.camera.capture(output, 'rgb')
        return output

    __server__ = PiCameraServer()

if __name__ == '__main__':
    from labrad import util
    util.runServer(__server__)