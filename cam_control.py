import os
import threading
import time
from io import BytesIO

import RPi.GPIO as gp
from picamera import PiCamera

from smbus import SMBus


class Cameras:
    def __init__(self):
        gp.setwarnings(False)
        gp.setmode(gp.BOARD)

        gp.setup(7, gp.OUT)
        gp.setup(11, gp.OUT)
        gp.setup(12, gp.OUT)

        self._i2c = SMBus(1)
        self._current_camera = 0
        self._select_camera(0)

        self._camera = self._init_camera()
        self._camera_lock = threading.Lock()

    def take_image(self, cam_index, format, exposure) -> BytesIO:
        with self._camera_lock:
            self._select_camera(cam_index)

            stream = BytesIO()
            self._camera.shutter_speed = exposure * 1000
            try:
                self._camera.capture(stream, format)
            except:
                return 0
            stream.seek(0)
            return stream

    _gpio_camera_map = {
        0: (False, False, True),
        1: (True, False, True),
        2: (False, True, False),
        3: (True, True, False),
    }

    def _init_camera(self):
        camera = PiCamera(framerate=5, sensor_mode=2)
        camera.resolution = (2592, 1944)

        camera.iso = 100
        # give the camera time to initialise
        time.sleep(2)

        camera.shutter_speed = camera.exposure_speed
        camera.exposure_mode = "off"
        g = camera.awb_gains
        camera.awb_mode = "off"
        camera.awb_gains = g

        return camera

    def _select_camera(self, index):
        if self._current_camera == index:
            return

        if index not in self._gpio_camera_map:
            raise ArgumentError("camera index must be between 0 and 3")

        gp.setup(7, gp.OUT)
        gp.setup(11, gp.OUT)
        gp.setup(12, gp.OUT)

        self._i2c = SMBus(1)


        self._i2c.write_byte_data(0x70, 0, index + 4)

        gpio7, gpio11, gpio12 = self._gpio_camera_map[index]
        gp.output(7, gpio7)
        gp.output(11, gpio11)
        gp.output(12, gpio12)

        self._current_camera = index
