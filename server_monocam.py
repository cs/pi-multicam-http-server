import asyncio
import os
from picamera import PiCamera
from fastapi import FastAPI
from fastapi.responses import Response
from PIL import Image
import numpy as np
import json
from json import JSONEncoder
import numpy



class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)




app = FastAPI()


cam = PiCamera()


def jsonify(data):
	json_data = dict()
	for key, value in data.iteritems():
		if isinstance(value, list): # for lists
			value = [ jsonify(item) if isinstance(item, dict) else item for item in value ]
		if isinstance(value, dict): # for nested lists
			value = jsonify(value)
		if isinstance(key, int): # if key is integer: > to string
			key = str(key)
		if type(value).__module__=='numpy': # if value is numpy.*: > to python list
			value = value.tolist()
		json_data[key] = value
	return json_data


@app.get("/take/{exposure}/{raw}")
async def main(exposure: int, raw: int):
	cam.shutter_speed = exposure

	cam.capture_sequence(['tmp.jpg'], use_video_port=True)
	
	if raw == 0:
		imf = open("tmp.jpg", "rb")
		
		try:
			return Response(imf.read(), media_type="image/jpeg")
		except:
			return 0
	else:
		img = Image.open('tmp.jpg')
		data = np.asarray(img)
		print(data)
		return Response(content=json.dumps(data, cls=NumpyArrayEncoder))
