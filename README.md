# pi-multicam-http-server

Http server that distributes images from individual cameras connected to a Raspberry Pi using the ArduCam 4 channel multicam module. 


## Setup

Clone this repository on a Raspberry Pi with the Arducam 4 channel multi-camera adapter (v2.2) connected.
Run `pipenv install` to install all dependencies and `pipenv run uvicorn server:app --host [[hostname / ip address of the raspberry pi]] --reload` to start the server.

## Usage

To make the Raspberry Pi take an image using the i-th camera and send it to you via http, call `http://[host]:8000/[camera index]/[exposure]`, where `[camera_index]` is an integer between 0 and 3 selecting the camera and `[exposure]` is the exposure time in milliseconds (int).

